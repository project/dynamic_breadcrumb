<?php

namespace Drupal\dynamic_breadcrumb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides settings form for dynamic breadcrumb.
 */
class DynamicBreadcrumbConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityManager = $container->get('entity.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dynamic_breadcrumb.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dynamic_breadcrumb_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dynamic_breadcrumb.settings');
    $entityTypes = [
      "Content type" => "node",
      'Taxonomy term' => "taxonomy_term",
      'Media' => "media",
      'User' => "user",
    ];
    foreach ($entityTypes as $key => $entityType) {
      $bundles = [];
      $bundlesInfo = $this->entityManager->getBundleInfo($entityType);
      foreach ($bundlesInfo as $bundleMachineName => $bundleInfo) {
        $bundles[$bundleMachineName] = $bundleInfo['label'];
      }
      if ($entityType === "taxonomy_term") {
        $entityType = "term";
      }

      $form[$entityType] = [
        '#type' => 'details',
        '#title' => $this->t($key . ' settings'),
        '#weight' => 5,
        '#open' => TRUE,
        '#tree' => TRUE,
      ];
      foreach ($bundles as $key => $bundle) {
        $bundleName = str_replace("_", '-', $key);
        $form[$entityType][$bundleName]["checkbox"] = [
          '#type' => 'checkbox',
          '#title' => $bundle,
          '#default_value' => $config->get($entityType . "." . $bundleName . ".checkbox"),
          '#attributes' => [
            'class ' => $bundleName . "_checkbox",
          ],
        ];
        $conditionalStates = [
          ':input[class=' . $bundleName . '_checkbox]' => ['checked' => TRUE],
        ];
        $form[$entityType][$bundleName]['value'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Value'),
          '#default_value' => $config->get($entityType . "." . $bundleName . ".value"),
          '#size' => 60,
          '#states' => [
            'visible' => $conditionalStates,
            'required' => $conditionalStates,
          ],
        ];
      }
      $form[$entityType]['token_tree'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$entityType],
        '#show_restricted' => TRUE,
        '#weight' => 9,
      ];

    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('dynamic_breadcrumb.settings');
    $formValues = $form_state->getValues();
    unset($formValues["submit"]);
    unset($formValues["form_build_id"]);
    unset($formValues["form_token"]);
    unset($formValues["form_id"]);
    unset($formValues["op"]);
    foreach ($formValues as $key => $formValue) {
      $config->set($key, $formValue);
    }
    $config->save();
    $this->messenger()->addMessage($this->t("The breadcrumb settings have been saved successfully!"));
  }

}
